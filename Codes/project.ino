#include <driver/i2s.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
#include "eyes.h"

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
#define OLED_RESET -1    // Reset pin # (or -1 if sharing Arduino reset pin)
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

// ESP32 I2C pins
#define SDA_PIN 21
#define SCL_PIN 22

#define I2S_WS 15
#define I2S_SD 13
#define I2S_SCK 2

#define I2S_PORT I2S_NUM_0

int n = 0;
int xp = 16;
int yp = 17;
int mood = 0;
int ko = 27;

enum State {
  IDLE,
  ACTIVE,
  COOLDOWN
};

State currentState = IDLE;
unsigned long stateStartTime = 0;

void setup() {
  Serial.begin(115200);
  pinMode(27, OUTPUT);
  pinMode(17, OUTPUT);
  pinMode(16, OUTPUT);
  pinMode(5, OUTPUT);
  digitalWrite(5, HIGH);
  Serial.println("Setup I2S ...");

  // Initialize I2C with ESP32-specific pins
  Wire.begin(SDA_PIN, SCL_PIN);

  if (!display.begin(SSD1306_SWITCHCAPVCC, 0x3C)) { // Address 0x3C for 128x64
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }

  // Clear the buffer and display an initial message
  display.clearDisplay();
  display.display();

  delay(1000);
  i2s_install();
  i2s_setpin();
  i2s_start(I2S_PORT);
  delay(500);
}

void loop() {
  int32_t sample = 0;
  size_t bytes_read = 0;

  static int xd = 0;
  static int espera = 0;
  static int step = 0;
  int x1, x2, y1, y2;

  if (espera > 0) {
    espera--;
  } else {
    x1 = xd + (xp > 16 ? (16 + 2 * (xp - 16)) : xp);
    x2 = 64 + xd + (xp < 16 ? (-16 + (xp * 2)) : xp);
    y1 = y2 = yp; // Keep y coordinates same for simplicity, can be changed for different positions

    switch (step) {
      case 0:
        display.clearDisplay(); // Clear the display buffer
        if (xp < 6) {
          display.drawBitmap(x1, y1, peyes[mood][2][0], 32, 32, WHITE);
          display.drawBitmap(x2, y2, peyes[mood][1][1], 32, 32, WHITE);
        } else if (xp < 26) {
          display.drawBitmap(x1, y1, peyes[mood][0][0], 32, 32, WHITE);
          display.drawBitmap(x2, y2, peyes[mood][0][1], 32, 32, WHITE);
        } else {
          display.drawBitmap(x1, y1, peyes[mood][1][0], 32, 32, WHITE);
          display.drawBitmap(x2, y2, peyes[mood][2][1], 32, 32, WHITE);
        }
        display.display();
        espera = random(250, 1000);
        n++;
        if (n == 2) {
          step = 1;
        }
        break;
      case 1:
        display.clearDisplay(); // Clear the display buffer
        display.drawBitmap(x1, y1, eye0, 32, 32, WHITE);
        display.drawBitmap(x2, y2, eye0, 32, 32, WHITE);
        display.display();
        espera = 100;
        step = 0;
        n = 0;
        break;
    }
  }

  // Use i2s_read to read data from the I2S port
  i2s_read(I2S_PORT, &sample, sizeof(sample), &bytes_read, portMAX_DELAY);

  if (bytes_read > 0) {
    Serial.print(20000000);
    Serial.print('\t');
    Serial.print(sample);
    Serial.print('\t');
    Serial.print(-20000000);
    Serial.print('\t');
    Serial.print(n);

    unsigned long currentTime = millis();

    switch (currentState) {
      case IDLE:
        if (sample >= 20000000 || sample <= -20000000) {
          currentState = ACTIVE;
          stateStartTime = currentTime;
          Serial.println("State: ACTIVE");
        }
        break;

      case ACTIVE:
        if (currentTime - stateStartTime >= 5000) {
          currentState = COOLDOWN;
          stateStartTime = currentTime;
          mood = 2; // or any other action you want during cooldown
          digitalWrite(27, LOW);
          digitalWrite(17, LOW);
          digitalWrite(16, LOW);
          digitalWrite(5, HIGH);
          Serial.println("State: COOLDOWN");
        } else {
          mood = 1;
          digitalWrite(27, HIGH);
          digitalWrite(17, HIGH);
          digitalWrite(16, HIGH);
          digitalWrite(5, LOW);
        }
        break;

      case COOLDOWN:
        if (currentTime - stateStartTime >= 5000) {
          currentState = IDLE;
          Serial.println("State: IDLE");
        }
        break;
    }

    Serial.println();
  }
}

void i2s_install() {
  const i2s_config_t i2s_config = {
    .mode = (i2s_mode_t)(I2S_MODE_MASTER | I2S_MODE_RX),
    .sample_rate = 44100,
    .bits_per_sample = I2S_BITS_PER_SAMPLE_16BIT,
    .channel_format = I2S_CHANNEL_FMT_ONLY_LEFT,
    .communication_format = (i2s_comm_format_t)(I2S_COMM_FORMAT_I2S | I2S_COMM_FORMAT_I2S_MSB),
    .intr_alloc_flags = 0, // default interrupt priority
    .dma_buf_count = 8,
    .dma_buf_len = 64,
    .use_apll = false
  };

  i2s_driver_install(I2S_PORT, &i2s_config, 0, NULL);
}

void i2s_setpin() {
  const i2s_pin_config_t pin_config = {
    .bck_io_num = I2S_SCK,
    .ws_io_num = I2S_WS,
    .data_out_num = -1,
    .data_in_num = I2S_SD
  };

  i2s_set_pin(I2S_PORT, &pin_config);
}